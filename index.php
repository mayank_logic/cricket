<?php

$file_content = file_get_contents('a.txt');

$array = explode("<tr class=",$file_content);

$playersArray	= array();
foreach($array as $key=>$value){
	
        	$tdArray	=	explode("<td class=",$value);
		
		foreach($tdArray as $key1=>$value1) {
			if($key1 == 2 || $key1 == 3 || $key1 == 4){
				$value1	= str_replace('"">',"",$value1);
				$value1	= str_replace('</td>',"",$value1);						
				$value1	= str_replace('<span class="CSKlogo"></span>',"",$value1);				
				$value1	= str_replace('<span class="MIlogo"></span>',"",$value1);
				$value1	= str_replace('<span class="DDlogo"></span>',"",$value1);
				$value1	= str_replace('<span class="KKRlogo"></span>',"",$value1);
				$value1	= str_replace('<span class="RCBlogo"></span>',"",$value1);
				$value1	= str_replace('<span class="RRlogo"></span>',"",$value1);
				$value1	= str_replace('<span class="KXIPlogo"></span>',"",$value1);
				$value1	= str_replace('<span class="SRHlogo"></span>',"",$value1);
				$value1	= str_replace('<div>',"",$value1);				
				$value1	= str_replace('</div>',"",$value1);
				if($key1 == 2){ //name
					$pArray	= explode('"',$value1);
					$name	= $pArray[1];
				}else if($key1 == 3){ //price
					$price  = trim($value1)/100000;
				}else if($key1 == 4){ //team
					$pArray	= explode('"',$value1);
					$team   = trim($value1);
				}else {

				}

			}
		}	
		$pArray	=	array("name"=>$name,"price"=>$price,"team"=>$team);
		array_push($playersArray,$pArray);
}
array_shift($playersArray);
//print_r($playersArray);
?>
<html>
	<head>
		
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
		
	</head>
	<body>
		<h1>Let's Play IPL 2018</h1>
		<h3>Selected Price : <span id="totalPrice">0</span>/50</h3>
		<table id="example" class="display" style="width:100%">
			<thead>
				<tr>
					<th>Name</th>
					<th>Price</th>
					<th>Team</th>
					<th>Action</th>
			    	</tr>
			</thead>
			<tbody>
				<?php 
				foreach($playersArray as $values){
					$name 	= $values['name'];
					$price 	= $values['price'];
					$team 	= $values['team'];
					echo "<tr><td>".$name."</td><td>".$price."</td><td>".$team."</td>";
					echo "<td><input type='checkbox' onclick=showTotalPrice(this,$price);></td></tr>";	
				}
				?>
			</tbody>
		</table>
	<body>
	<script src="js/jquery-1.12.4.js"></script>
	<script src="js/jquery.dataTables.min.js "></script>
	<script>
	$('#example').DataTable({ "pageLength": 25});
	function showTotalPrice(thisObj,price){
		var oldVal = parseFloat($("#totalPrice").html(),10);
		
		var newVal =	0;	
		if($(thisObj).prop("checked") == true){
			newVal = oldVal + price;
		}else {
			newVal = oldVal - price;
		}
		if(newVal <= 50 ){
			$("#totalPrice").html(newVal);
		}else {
			$(thisObj).prop('checked', false);
			alert("More than 50 not allowed");
		}
	}
	</script>

</html>

